job "demo" {
  type = "service"
  datacenters = ["dc1"]
  meta {
    git_sha = "[[.git_sha]]"
  }
  group "demo" {
    count = 2

    update {
      max_parallel = 2
      canary = 2
    }

    task "server" {
      env {
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }

      driver = "exec"
   
      artifact {
        source = "[[.artifact_url]]"
      }

      config {
        command = "demo-webapp"
      }

      resources {
        cpu = 128
        memory = 128
        network {
          mbits = 10
          port  "http"{}
        }
      }

      service {
        name = "demo-canary"
        port = "http"

        tags = []
        canary_tags = [
          "traefik.enable=true",
          "traefik.tags=service",
          "traefik.frontend.rule=Host:canary.demo.dev",
        ]

        check {
          type = "http"
          path = "/"
          interval = "5s"
          timeout = "1s"
        }
      }

      service {
        name = "demo"
        port = "http"

        tags = [
          "traefik.enable=true",
          "traefik.tags=service",
          "traefik.frontend.rule=Host:demo.dev"
        ]
        canary_tags = [
          "traefik.enable=true"
        ]

        check {
          type = "http"
          path = "/"
          interval = "5s"
          timeout = "1s"
        }
      }

    }
  }
}

