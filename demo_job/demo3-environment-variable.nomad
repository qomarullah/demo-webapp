vault token 
root/token=s.MdSH8InU1Qurw8BJrhx8Kjol
key1=/phrcstuSOMkg5GbmmgRbczpMIvBsPdjFc2A7iB2468=


# creat policy nomad-server
root@app1:/etc/nomad.d# vault token create -address="http://127.0.0.1:8200" -policy nomad-server -period 72h -orphan

Key                  Value
---                  -----
token                s.qLc8oOWxtDYjwxbNA2p6ZAAv
token_accessor       OWcnFQNtxVrP6zxcdaaQqBNf
token_duration       72h
token_renewable      true
token_policies       ["default" "nomad-server"]
identity_policies    []
policies             ["default" "nomad-server"]

export VAULT_TOKEN=s.qLc8oOWxtDYjwxbNA2p6ZAAv




job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "8090"
      }
    }
	service {
		name="webapi"
	}
    task "server" {
        vault {
            policies = ["nomad-server"]
            change_mode = "restart"
            //change_mode   = "signal"
            //change_signal = "SIGUSR1"
           // env = true
       }
        

        template {
            data = <<EOH
            # Lines starting with a # are ignored
            # Empty lines are also ignored
            LOG_LEVEL="{{key "service/webapi/log-level"}}"
            API_KEY="{{with secret "secret/webapi"}}{{.Data.api_key}}{{end}}"
            EOH
            destination = "secrets/file.env"
            change_mode   = "restart"
            env         = true
        }

	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.2"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	 
  	}
  }
}



job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "8090"
      }
    }
	service {
		name="webapi"
	}
    task "server" {
        vault {
            policies = ["nomad-server"]
          
       }
        

        template {
            data = <<EOH
            # Lines starting with a # are ignored
            # Empty lines are also ignored
            LOG_LEVEL="{{key "service/webapi/log-level"}}"
            API_KEY="{{with secret "kvttl/webapi"}}{{.Data.api_key}}{{end}}"
            EOH
            destination = "secrets/file.env"
            //change_mode   = "restart"
            env         = true
        }

	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.2"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	 
  	}
  }
}




job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "8090"
      }
    }
	service {
		name="webapi"
	}
    task "server" {
        vault {
          policies = ["nomad-server"]
      }
      
     artifact {
        source = "http://3272ebe29ad1.ngrok.io/assets/file.env.tpl"
      }
    
      template {
        source      = "local/file.env.tpl"
        destination = "xxx/file.env"
        env = true
      }

        

	  env {
		    NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
        image = "docker.io/qomarullah/go-server:v0.1.2"
        ports = ["http"]
        auth {
          username = "qomarullah"
          password = "qazwsx"
        }
        volumes= [
           "xxx/file.env:/yyy/file.env"
        ]
	   }
	 
  	}
  }
}



docker inspect 580c37406ad5 | jq '.[0].HostConfig.Binds'
docker exec -it a7c84604d5a7 /bin/sh




export VAULT_TOKEN=s.qLc8oOWxtDYjwxbNA2p6ZAAv
export VAULT_ADDR=http://localhost:8200
export VAULT_TOKEN=s.MdSH8InU1Qurw8BJrhx8Kjol


