
#server
34.101.236.16
34.101.149.51
34.101.121.224

jenkins :34.101.81.30
sudo gitlab-runner register -n --url https://gitlab.com --registration-token RHeH8hdSGN9_LZg58kTZ --executor docker --description "Deployment Runner" --docker-image "docker:stable" --tag-list deployment --docker-privileged



#static port
job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 4
    network {
      port "http" {
        static = "10000"
		to = "8090"
      }
    }
	service {
		name="webapi"
	}
    task "server" {
	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.2"
		ports = ["http"]
	  }
	
  	}
  }
}

#random port
job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 4
    network {
      port "http" {
        to = "8090"
      }
    }
	service {
		name="webapi"
		port = "http"
	}
    task "server" {
	env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.0"
		ports = ["http"]
	
	  }
	 
  	}
  }
}

# count update to 3
job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 3
    network {
      port "http" {
        to = "8090"
      }
    }
	service {
		name="webapi"
	}
    task "server" {
	  env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.0"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	 
  	}
  }
}

# tambah LB, traefik
job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 3
    network {
      port "http" {
        to = "8090"
      }
    }
	service {
		name="webapi"
		port = "http"
		tags = [
			"traefik.enable=true",
			"traefik.http.routers.http.rule=Path(`/hello`)",
		]
	}
    task "server" {
	env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.0"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	 
  	}
  }
}


job "traefik" {
  region      = "global"
  datacenters = ["dc1"]
  type        = "service"

  group "traefik" {
    count = 1

    network {
      port "http" {
        static = 8080
      }

      port "api" {
        static = 8081
      }
    }

    service {
      name = "traefik"

      check {
        name     = "alive"
        type     = "tcp"
        port     = "http"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "traefik" {
      driver = "docker"

      config {
        image        = "traefik:v2.2"
        network_mode = "host"

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
        ]
      }

      template {
        data = <<EOF
[entryPoints]
    [entryPoints.http]
    address = ":8080"
    [entryPoints.traefik]
    address = ":8081"

[api]
    dashboard = true
    insecure  = true

# Enable Consul Catalog configuration backend.
[providers.consulCatalog]
    prefix           = "traefik"
    exposedByDefault = false

    [providers.consulCatalog.endpoint]
      address = "127.0.0.1:8500"
      scheme  = "http"
EOF

        destination = "local/traefik.toml"
      }
      resources {
        cpu    = 100
        memory = 128
      }
    }
  }
}

--- lihat traefik dashboard 
--- topology cpu & mem




#restart
job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 3
    network {
      port "http" {
        to = "8090"
      }
    }
	restart {
		attempts = 2
		interval = "10m"
		delay = "10s"
		mode = "fail"
	}
	service {
		name="webapi"
		port = "http"
		tags = [
			"traefik.enable=true",
			"traefik.http.routers.http.rule=Path(`/hello`)",
		]
	}
    task "server" {
	env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.1"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	}
  }
}


# count 3, set unhealthy-> restart
job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 3
    network {
      port "http" {
        to = "8090"
      }
    }
	restart {
		attempts = 2
		interval = "10m"
		delay = "10s"
		mode = "fail"
	}
	service {
		name="webapi"
		port = "http"
		tags = [
			"traefik.enable=true",
			"traefik.http.routers.http.rule=Path(`/hello`)",
		]
		check {
			name     = "health using http endpoint '/health'"
			port     = "http"
			type     = "http"
			path     = "/health"
			method   = "GET"
			interval = "10s"
			timeout  = "2s"
		}
		check_restart {
          limit = 2
          grace = "10s"
          ignore_warnings = false
        }
	}
    task "server" {
	 env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.0"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	}
  }
}
-- kill 1
-- check restart


#reschedue
job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 3
    network {
      port "http" {
        to = "8090"
      }
    }
	restart {
		attempts = 2
		interval = "10m"
		delay = "10s"
		mode = "fail"
	}
	reschedule {
		delay          = "30s"
		delay_function = "exponential"
		max_delay      = "1hr"
		unlimited      = true
	}
	service {
		name="webapi"
		port = "http"
		tags = [
			"traefik.enable=true",
			"traefik.http.routers.http.rule=Path(`/hello`)",
		]
		check {
			name     = "health using http endpoint '/health'"
			port     = "http"
			type     = "http"
			path     = "/health"
			method   = "GET"
			interval = "10s"
			timeout  = "2s"
		}
		check_restart {
          limit = 2
          grace = "10s"
          ignore_warnings = false
        }
	}
    task "server" {
	 env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.0"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	}
  }
}

#canary
job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 2
    network {
      port "http" {
        to = "8090"
      }
    }
	restart {
		attempts = 2
		interval = "10m"
		delay = "10s"
		mode = "fail"
	}
	update {
		canary            = 2
		max_parallel      = 1
		health_check      = "checks"
		min_healthy_time  = "10s"
		healthy_deadline  = "5m"
		progress_deadline = "10m"
		auto_revert       = true
    }
	service {
		name="webapi"
		port = "http"
		tags = [
			"traefik.enable=true",
			"traefik.http.routers.http.rule=Path(`/hello`)",
		]
		check {
			name     = "health using http endpoint '/health'"
			port     = "http"
			type     = "http"
			path     = "/health"
			method   = "GET"
			interval = "10s"
			timeout  = "2s"
		}
		check_restart {
          limit = 2
          grace = "10s"
          ignore_warnings = false
        }
	}
    task "server" {
	 env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.2"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	}
  }
}